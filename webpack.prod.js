const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const plugins = [
  new ExtractTextPlugin({ filename: 'css/[name].css', allChunks: true }),
  new HtmlWebpackPlugin({
    hash: true,
    title: 'Blog for GitHub Gists',
    template: './src/shared/index.html',
    filename: './index.html'
  })
];

module.exports = env => {
  if (env.NODE_ENV === 'production') {
    plugins.push(new CleanWebpackPlugin(['public']));
  }
  return {
    mode: 'production',
    entry: {
      app: ['babel-polyfill', path.join(__dirname, 'src/app/index.js')]
    },
    output: {
      path: path.resolve(__dirname, 'public'),
      filename: 'js/[name].js'
    },
    optimization: {
      splitChunks: {
        chunks: 'all'
      }
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['env', 'react', 'stage-2'],
              plugins: ['react-hot-loader/babel']
            }
          }
        },
        {
          test: /\.(scss|sass)$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: {
                  minimize: true,
                  modules: true
                }
              },
              'sass-loader'
            ]
          })
        },
        {
          test: /\.(jpg|png|gif|svg)$/,
          use: {
            loader: 'url-loader',
            options: {
              limit: 10000,
              fallback: 'file-loader',
              name: 'images/[name].[hash].[ext]'
            }
          }
        }
      ]
    },
    plugins
  };
};
