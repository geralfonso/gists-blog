export const FETCH_GISTS = 'FETCH_GISTS';
export const FETCH_GISTS_START = 'FETCH_GISTS_START';
export const RECEIVE_GISTS = 'RECEIVE_GISTS';
export const RECEIVE_SINGLE_GIST = 'RECEIVE_SINGLE_GIST';
export const FETCH_GISTS_ERROR = 'FETCH_GISTS_ERROR';
export const TOGGLE_MENU = 'TOGGLE_MENU';
export const FILTER_GISTS = 'FILTER_GISTS';
