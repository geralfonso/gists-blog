import {
  FETCH_GISTS_START,
  FETCH_GISTS_ERROR,
  RECEIVE_SINGLE_GIST
} from '../actions-types';

const initialState = {
  fetching: false,
  fetched: false,
  error: null,
  data: {}
};

function singleGist(state = initialState, action) {
  switch (action.type) {
    case FETCH_GISTS_START: {
      return { ...state, fetching: true };
    }
    case FETCH_GISTS_ERROR: {
      return { ...state, fetching: false, error: action.payload };
    }
    case RECEIVE_SINGLE_GIST: {
      return {
        ...state,
        fetching: false,
        fetched: true,
        data: action.payload.files[Object.keys(action.payload.files)[0]]
      };
    }
    default:
      return state;
  }
}

export default singleGist;
