import { TOGGLE_MENU } from '../actions-types';

const initialState = {
  visibility: true
};

function toggleMenu(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_MENU: {
      const visibility = !state.visibility;
      return { ...state, visibility };
    }
    default:
      return state;
  }
}

export default toggleMenu;
