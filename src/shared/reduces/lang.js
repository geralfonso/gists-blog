import { RECEIVE_GISTS } from '../actions-types';

const initialState = {
  data: []
};

function lang(state = initialState, action) {
  switch (action.type) {
    case RECEIVE_GISTS: {
      const unique = new Set();
      action.payload.map(gist => {
        if (gist.files[Object.keys(gist.files)[0]].language === null) {
          unique.add('Other');
        } else {
          unique.add(gist.files[Object.keys(gist.files)[0]].language);
        }
      });
      return {
        ...state,
        data: [...unique].sort()
      };
    }
    default:
      return state;
  }
}

export default lang;
