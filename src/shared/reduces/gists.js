import {
  FETCH_GISTS_START,
  FETCH_GISTS_ERROR,
  RECEIVE_GISTS,
  FILTER_GISTS
} from '../actions-types';

const initialState = {
  error: null,
  data: [],
  dataUpdate: []
};

function gists(state = initialState, action) {
  switch (action.type) {
    case FETCH_GISTS_START: {
      return { ...state };
    }
    case FETCH_GISTS_ERROR: {
      return { ...state, error: action.payload };
    }
    case RECEIVE_GISTS: {
      return {
        ...state,
        fetching: false,
        fetched: true,
        data: action.payload
      };
    }
    case FILTER_GISTS: {
      const list = state.data.filter(gist => {
        if (
          gist.files[Object.keys(gist.files)[0]].language !== null &&
          gist.files[Object.keys(gist.files)[0]].language === action.payload
        ) {
          return true;
        } else if (
          gist.files[Object.keys(gist.files)[0]].language === null &&
          action.payload === 'Other'
        ) {
          return true;
        }
      });

      return { ...state, dataUpdate: list };
    }
    default:
      return state;
  }
}

export default gists;
