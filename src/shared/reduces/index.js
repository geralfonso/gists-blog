import { combineReducers } from 'redux';
import lang from './lang';
import gists from './gists';
import singleGist from './singleGist';
import toggleMenu from './toggleMenu';

const rootReducer = combineReducers({
  gists,
  lang,
  singleGist,
  toggleMenu
});

export default rootReducer;
