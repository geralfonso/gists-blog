import Axios from 'axios';
import {
  FETCH_GISTS,
  RECEIVE_GISTS,
  FETCH_GISTS_ERROR,
  RECEIVE_SINGLE_GIST,
  TOGGLE_MENU,
  FILTER_GISTS
} from '../actions-types';

export function toggleMenu() {
  return {
    type: TOGGLE_MENU
  };
}
export function filterGists(query) {
  return {
    type: FILTER_GISTS,
    payload: query
  };
}

function gistsError(err) {
  return {
    type: FETCH_GISTS_ERROR,
    payload: err
  };
}
function receiveGists(data) {
  return {
    type: RECEIVE_GISTS,
    payload: data
  };
}
function fetchGists() {
  return {
    type: FETCH_GISTS
  };
}

function receiveSingleGist(data) {
  return {
    type: RECEIVE_SINGLE_GIST,
    payload: data
  };
}

// export function getGists() {
//   return dispatch => {
//     dispatch(fetchGists());
//     return Axios.get('https://api.github.com/gists/public')
//       .then(res => {
//         dispatch(receiveGists(res.data));
//       })
//       .catch(err => {
//         dispatch(gistsError(err));
//       });
//   };
// }

export function getGists(url) {
  return async dispatch => {
    try {
      dispatch(fetchGists());
      let res = '';
      if (url === undefined) {
        res = await Axios.get('https://api.github.com/gists/public');
        return dispatch(receiveGists(res.data));
      } else {
        res = await Axios.get(url);
        return dispatch(receiveSingleGist(res.data));
      }
    } catch (err) {
      dispatch(gistsError(err));
    }
  };
}
