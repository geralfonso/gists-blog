import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NavContainer from '../components/NavContainer';
import DropDown from '../components/Dropdown';
import { connect } from 'react-redux';
import { toggleMenu } from '../../../shared/actions/index';

class Navbar extends Component {
  handleToggleMenu = () => {
    this.props.dispatch(toggleMenu());
    this.props.toggleMenu
      ? (this.navbarMenu.style.display = 'block')
      : this.navbarMenu.removeAttribute('style');
  };

  setToggleRef = e => {
    this.navbarMenu = e;
  };

  render() {
    return (
      <NavContainer
        handleToggleMenu={this.handleToggleMenu}
        toggleRef={this.setToggleRef}
      >
        <DropDown
          lang={this.props.lang}
          handleFilterClick={this.props.handleFilterClick}
        />
      </NavContainer>
    );
  }
}

Navbar.propTypes = {
  lang: PropTypes.array,
  dispatch: PropTypes.func,
  handleFilterClick: PropTypes.func,
  toggleMenu: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    toggleMenu: state.toggleMenu.visibility
  };
}

export default connect(mapStateToProps)(Navbar);
