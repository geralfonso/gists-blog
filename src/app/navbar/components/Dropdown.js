import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

function DropDown(props) {
  return (
    <div className="navbar-item has-dropdown is-hoverable">
      <Link to="/" className="navbar-link">
        Language
      </Link>
      <div className="navbar-dropdown is-boxed">
        <Link
          to="/"
          className="navbar-item is-active"
          onClick={props.handleFilterClick}
        >
          All
        </Link>
        <hr className="navbar-divider" />
        {props.lang.map((lang, i) => {
          return (
            <Link
              key={i}
              className="navbar-item"
              onClick={props.handleFilterClick}
              to="/"
            >
              {lang}
            </Link>
          );
        })}
      </div>
    </div>
  );
}

DropDown.propTypes = {
  lang: PropTypes.array,
  handleFilterClick: PropTypes.func
};

export default DropDown;
