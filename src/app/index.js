import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import './sass/style.sass';
import configureStore from '../shared/store/configureStore';
import { BrowserRouter as Router } from 'react-router-dom';

const store = configureStore(),
  rootElement = document.getElementById('root');

render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  rootElement
);
