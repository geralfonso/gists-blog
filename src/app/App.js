import React, { Component } from 'react';
import { hot } from 'react-hot-loader';
import { Route, Switch, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Home from './home/containers/home';
import Navbar from './navbar/containers/navbar';
import { connect } from 'react-redux';
import { getGists, filterGists } from '../shared/actions';
import Post from './post/containers/post';
import Footer from './footer/containers/Footer';

class App extends Component {
  componentDidMount() {
    // this.props.dispatch(dispatch => {
    //   dispatch({ type: 'FETCH_GISTS' });
    //   Axios.get('https://api.github.com/gists/public')
    //     .then(res => {
    //       dispatch({ type: 'RECEIVE_GISTS', payload: res.data });
    //     })
    //     .catch(err => {
    //       dispatch({ type: 'FETCH_GISTS_ERROR', payload: err });
    //     });
    // });
    this.props.dispatch(getGists());
  }
  handleFilterClick = e => {
    this.props.dispatch(filterGists(e.target.innerText));
  };
  render() {
    return (
      <div>
        <Navbar
          lang={this.props.lang}
          visivility={this.props.visivility}
          handleFilterClick={this.handleFilterClick}
        />
        <Switch>
          <Route
            exact
            path="/"
            render={props => <Home {...props} data={this.props.data} />}
          />
          <Route
            path="/:gistId"
            render={props => (
              <Post {...props} handleFilterClick={this.handleFilterClick} />
            )}
          />
        </Switch>
        <Footer />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const data =
    state.gists.dataUpdate.length > 0
      ? state.gists.dataUpdate
      : state.gists.data;
  return {
    data: data,
    lang: state.lang.data,
    visivility: state.gists.visivility
  };
}

App.propTypes = {
  data: PropTypes.array,
  lang: PropTypes.array,
  dispatch: PropTypes.func,
  visivility: PropTypes.bool
};

export default hot(module)(withRouter(connect(mapStateToProps)(App)));
