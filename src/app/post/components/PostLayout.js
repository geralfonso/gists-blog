import React from 'react';
import PropTypes from 'prop-types';

function PostLayout(props) {
  return (
    <main className="bd-main">
      <div className="bd-side-background" />
      <div className="bd-main-container container">
        <div className="bd-duo">{props.children}</div>
      </div>
    </main>
  );
}

PostLayout.propTypes = {
  children: PropTypes.node
};

export default PostLayout;
