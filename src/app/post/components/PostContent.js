import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

function PostContent(props) {
  return (
    <div className="bd-lead">
      <div className="bd-breadcrumb">
        <nav className="breadcrumb" aria-label="breadcrumbs">
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/" onClick={props.handleFilterClick}>
                {props.lang === null ? 'Other' : props.lang}
              </Link>
            </li>
            <li className="is-active">
              <a href="">{props.filename}</a>
            </li>
          </ul>
        </nav>
      </div>
      <header className="bd-header">
        <div className="bd-header-titles">
          <h1 className="title">{props.filename}</h1>
          <p className="subtitle is-4">Updated at: {props.update}</p>
        </div>
      </header>
      <div className="bd-content">
        <div className="columns">
          <div className="column is-offset-2 is-8">
            <div className="bd-post content is-medium">
              <p>{props.des}</p>
              <div id="meta" className="field is-grouped is-grouped-multiline">
                <div className="control">
                  <div className="tags has-addons">
                    <span className="tag">User</span>

                    <a
                      className="tag is-link"
                      href={props.url_user}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {props.user}
                    </a>
                  </div>
                </div>
                <div className="control">
                  <div className="tags has-addons">
                    <span className="tag">Repo</span>

                    <a
                      className="tag is-link"
                      href={props.url_repo}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {props.filename}
                    </a>
                  </div>
                </div>

                <div className="control">
                  <div className="tags has-addons">
                    <span className="tag">Size</span>
                    <span className="tag is-primary">{props.size}</span>
                  </div>
                </div>

                <div className="control">
                  <div className="tags has-addons">
                    <span className="tag">Language</span>
                    <span className="tag is-primary">
                      {props.lang === null ? 'Other' : props.lang}
                    </span>
                  </div>
                </div>
              </div>
              <pre>
                <code>{props.content}</code>
              </pre>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

PostContent.propTypes = {
  update: PropTypes.string,
  des: PropTypes.string,
  content: PropTypes.string,
  filename: PropTypes.string,
  lang: PropTypes.string,
  handleFilterClick: PropTypes.func,
  url_user: PropTypes.string,
  url_repo: PropTypes.string,
  user: PropTypes.string,
  size: PropTypes.number
};
export default PostContent;
