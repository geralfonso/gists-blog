import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getGists } from '../../../shared/actions';
import PostLayout from '../components/PostLayout';
import PostContent from '../components/PostContent';

class Post extends Component {
  componentDidMount() {
    this.props.dispatch(getGists(this.props.thisGist.url));
  }
  getFormatedFilename() {
    const str = this.props.fetchGist.filename;
    if (str !== undefined) {
      const lidx = str.lastIndexOf('.');
      const newStr = str.slice(0, lidx);
      const strNoCase = newStr.replace(/[^a-zA-Z0-9]/g, ' ');
      const strCapital = strNoCase.charAt(0).toUpperCase() + strNoCase.slice(1);
      return strCapital;
    }
  }
  getFormatedDate() {
    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    const months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
    ];
    const d = new Date(this.props.thisGist.updated_at);

    return `${days[d.getDay()]}, ${
      months[d.getMonth()]
    } ${d.getDate()} ${d.getFullYear()}`;
  }
  render() {
    return (
      <PostLayout>
        <PostContent
          filename={this.getFormatedFilename()}
          content={this.props.fetchGist.content}
          des={this.props.thisGist.description}
          update={this.getFormatedDate()}
          lang={this.props.fetchGist.language}
          size={this.props.fetchGist.size}
          user={this.props.thisGist.owner.login}
          url_user={this.props.thisGist.owner.html_url}
          url_repo={this.props.thisGist.html_url}
          handleFilterClick={this.props.handleFilterClick}
        />
      </PostLayout>
    );
  }
}

function mapStateToProps(state, props) {
  const gist = state.gists.data.find(
    ({ id }) => id === props.match.params.gistId
  );
  return {
    thisGist: gist,
    fetchGist: state.singleGist.data
  };
}

Post.propTypes = {
  data: PropTypes.array,
  thisGist: PropTypes.object,
  handleFilterClick: PropTypes.func,
  fetchGist: PropTypes.object,
  dispatch: PropTypes.func
};

export default withRouter(connect(mapStateToProps)(Post));
