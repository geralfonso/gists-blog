import React, { Component } from 'react';
import Card from '../components/card';
import HomeContainer from '../components/HomeContainer';
import PropTypes from 'prop-types';

class Home extends Component {
  findDemoUrl = str => {
    const unRegex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi; // Change this line
    if (str === null) {
      return { __html: '' };
    } else if (unRegex.test(str)) {
      let res = str.replace(
        unRegex,
        `<a href="${str.match(
          unRegex
        )}"target="_blank" rel="noopener noreferrer">Demo</a>`
      );
      return { __html: res };
    }
    return { __html: `<p>${str}</p>` };
  };

  getFormatedDate(date) {
    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    const months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
    ];
    const d = new Date(date);

    return `${days[d.getDay()]}, ${
      months[d.getMonth()]
    } ${d.getDate()} ${d.getFullYear()}`;
  }

  render() {
    return (
      <HomeContainer>
        {this.props.data.map((gist, i) => {
          return (
            <Card
              findDemoUrl={this.findDemoUrl}
              gist={gist}
              key={i}
              getFormatedDate={this.getFormatedDate(gist.updated_at)}
            />
          );
        })}
      </HomeContainer>
    );
  }
}

Home.propTypes = {
  data: PropTypes.array
};

export default Home;
