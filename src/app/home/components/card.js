import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

function Card(props) {
  return (
    <div className="column is-mobile is-half-tablet is-one-third-desktop is-one-third-widescreen">
      <div className="card">
        <Link to={`/${props.gist.id}`}>
          <div className="card-image">
            <figure className="image is-4by3">
              <img src={props.gist.owner.avatar_url} alt="Placeholder image" />
            </figure>
          </div>
        </Link>
        <div className="card-content">
          <div className="media">
            <div className="media-left">
              <figure className="image is-48x48">
                <img
                  src={props.gist.owner.avatar_url}
                  alt="Placeholder image"
                />
              </figure>
            </div>
            <div className="media-content">
              <p className="title is-4">{props.gist.owner.login}</p>
              <p className="subtitle is-6">
                {props.gist.files[Object.keys(props.gist.files)[0]].language ===
                null
                  ? 'Other'
                  : props.gist.files[Object.keys(props.gist.files)[0]].language}
              </p>
            </div>
          </div>

          <div className="content">
            <p className="subtitle is-6">
              <span className="tag is-primary">File Name</span>{' '}
              {props.gist.files[Object.keys(props.gist.files)[0]].filename}
            </p>
            <p
              dangerouslySetInnerHTML={props.findDemoUrl(
                props.gist.description
              )}
            />

            <a
              href={props.gist.owner.html_url}
              target="_blank"
              rel="noopener noreferrer"
            >
              Profile
            </a>
            <span> and </span>
            <a
              href={props.gist.html_url}
              target="_blank"
              rel="noopener noreferrer"
            >
              Repo
            </a>
            <br />
            <time dateTime="2016-1-1">{props.getFormatedDate}</time>
          </div>
        </div>
      </div>
    </div>
  );
}

Card.propTypes = {
  gist: PropTypes.object,
  findDemoUrl: PropTypes.func,
  getFormatedDate: PropTypes.string
};
export default Card;
