import React from 'react';
import PropTypes from 'prop-types';

function HomeContainer(props) {
  return (
    <section className="bd-tws-home section is-medium">
      <header className="bd-index-header">
        <h3 className="title is-3">
          <a href="">
            A blog for <strong>GitHub Gists</strong>
          </a>
        </h3>
        <h4 className="subtitle is-4">See what GitHub community is building</h4>
      </header>
      <div className="container">
        <div className="columns is-multiline is-mobile">{props.children}</div>
      </div>
    </section>
  );
}

HomeContainer.propTypes = {
  children: PropTypes.node
};

export default HomeContainer;
