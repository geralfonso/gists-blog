import React from 'react';
import PropTypes from 'prop-types';

function FooContainer(props) {
  return (
    <footer className="footer">
      <div className="content has-text-centered">{props.children}</div>
    </footer>
  );
}

FooContainer.propTypes = {
  children: PropTypes.node
};
export default FooContainer;
