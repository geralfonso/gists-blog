import React from 'react';
import PropTypes from 'prop-types';

function FooContent() {
  return (
    <p>
      <strong>This Blog for GitHub Gists</strong> is made by{' '}
      <a
        href="https://github.com/geralfonso"
        target="_blank"
        rel="noopener noreferrer"
      >
        German Alfonso
      </a>.
    </p>
  );
}

FooContent.propTypes = {
  children: PropTypes.node
};
export default FooContent;
