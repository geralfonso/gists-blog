import React, { Component } from 'react';
import FooContainer from '../components/FooContainer';

class Footer extends Component {
  render() {
    return (
      <FooContainer>
        <p>
          <strong>This Blog for GitHub Gists</strong> is made by{' '}
          <a href="https://jgthms.com">German Alfonso</a>.
        </p>
      </FooContainer>
    );
  }
}

export default Footer;
